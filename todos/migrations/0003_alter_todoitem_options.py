# Generated by Django 5.0 on 2023-12-13 17:28

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("todos", "0002_todoitem"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="todoitem",
            options={"ordering": ["task"]},
        ),
    ]
